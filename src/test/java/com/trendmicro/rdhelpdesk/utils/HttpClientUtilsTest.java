package com.trendmicro.rdhelpdesk.utils;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trendmicro.rdhelpdesk.ws.codesign.CodeSign;
import com.trendmicro.rdhelpdesk.ws.data.CodeSignCondition;
import com.trendmicro.rdhelpdesk.ws.data.PerforceCondition;
import com.trendmicro.rdhelpdesk.ws.perforce.P4AccountActivation;

public class HttpClientUtilsTest {
	private static final Logger LOG = LoggerFactory.getLogger(HttpClientUtilsTest.class);

	@Test
	public void testCodeSign() {
		LOG.debug("testCodeSign start");
		// LOG.debug("helloWorldServlet bean is {}", helloWorldServlet);
		CodeSignCondition condition = new CodeSignCondition();
		CodeSign cs = new CodeSign();
		condition.setItemId("13189");
		condition.setProductSite("TW");
		condition.setProductName("test");
		condition.setProductVersion("1.0");
		condition.setCertType("Trend");
		condition.setFileType("Normal file");
		condition.setBuildNumber("1001");
		condition.setPurpose("for test");
		// StringEscapeUtils.escapeJava("");
		condition.setRequestor("Allan Sung (RD-TW)");
		condition.setApprovingManager("Allan Sung (RD-TW)");
		condition.setFilePath("\\\\tw-ets-fs\\UnsignedFileRoot\\Allan\\unsign_test\\TmListen_64x.exe");
		String result = cs.getCodeSignSoap().requestSign(condition.getItemId(), condition.getProductSite(),
		        condition.getProductName(), condition.getProductVersion(), condition.getCertType(),
		        condition.getFileType(), condition.getBuildNumber(), condition.getPurpose(), condition.getRequestor(),
		        condition.getApprovingManager(), condition.getFilePath());
		// testCodeSign result is Error to trigger sign:Sign request rejectted
		LOG.debug("testCodeSign result is {}", result);
	}

	// @Test
	public void testPerforceReactive() {
		LOG.debug("testPerforceReactive start");
		PerforceCondition condition = new PerforceCondition();
		condition.setItemId("12345");
		condition.setUserName("Allan Sung (RD-TW)");
		P4AccountActivation pa = new P4AccountActivation();
		String result = pa.getP4AccountActivationSoap().requestActivation(condition.getItemId(),
		        condition.getUserName());
		// result is Failed to trigger p4 account activation request:P4 account
		// reactivation request rejectted
		LOG.debug("testPerforceReactive result is {}", result);
	}
}
