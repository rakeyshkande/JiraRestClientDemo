package com.trendmicro.rdhelpdesk.command.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.JiraRestClientFactory;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;

public class ConnectJiraUtils {
	private static final Logger LOG = LoggerFactory.getLogger(ConnectJiraUtils.class);
	private static final String JIRA_URL = "http://10.28.9.101:8080/";

	public static JiraRestClient GetJiraRestClientInstance(String username, String password) throws URISyntaxException {
		JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
		URI uri = new URI(JIRA_URL);
		return factory.createWithBasicHttpAuthentication(uri, username, password);
	}

	public static HttpURLConnection urlConnection(URL url, String encoding, boolean isTransition) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/json");
		if (isTransition) {
			connection.setRequestMethod("POST");
		} else {
			connection.setRequestMethod("PUT");
		}
		connection.setRequestProperty("Authorization", "Basic " + encoding);
		return connection;
	}

	public static HttpURLConnection urlGetConnection(URL url, String encoding) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Authorization", "Basic " + encoding);
		return connection;
	}

	public static URL forIssue(Issue issue, boolean isTransition) throws MalformedURLException {
		if (isTransition) {
			return new URL(issue.getSelf().toURL().toString() + "/transitions?expand=transitions.fields ");
		}
		return issue.getSelf().toURL();
	}

	public static String withEncoding(String user, String pass) {
		String userPassword = user + ":" + pass;
		return encodeBase64String(userPassword.getBytes());
	}

	public static String encodeBase64String(byte[] bytes) {
		// TODO Auto-generated method stub
		return Base64.encodeBase64String(bytes);
	}

	public static void update(Issue issue, JSONObject jsonObj, boolean isTransition, String user, String pass)
	        throws JSONException {
		try {
			HttpURLConnection connection = urlConnection(forIssue(issue, isTransition), withEncoding(user, pass),
			        isTransition);
			LOG.info("for Issue {}", forIssue(issue, isTransition));
			connection.connect();
			writeData(connection, jsonObj);
			checkResponse(connection);
		} catch (IOException e) {
			// LOG.info(e.getStackTrace().toString());
			throw new RuntimeException(e);
		}
	}

	private static void writeData(HttpURLConnection connection, JSONObject fields) throws IOException {
		// LOG.info(fields.toString());
		OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
		out.write(fields.toString());
		out.flush();
		out.close();
	}

	private static void checkResponse(HttpURLConnection connection) throws IOException {
		if (HttpURLConnection.HTTP_NO_CONTENT != connection.getResponseCode()) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
			        ((HttpURLConnection) (new URL(connection.getURL().toString())).openConnection()).getInputStream(),
			        Charset.forName("UTF-8")));

			StringBuilder stringBuilder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line + "\r\n");
			}
			LOG.info(stringBuilder.toString());
		}
		System.exit(0);
	}
}
