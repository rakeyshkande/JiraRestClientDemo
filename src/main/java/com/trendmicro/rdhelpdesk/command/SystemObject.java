package com.trendmicro.rdhelpdesk.command;

import com.atlassian.jira.rest.client.JiraRestClient;

public class SystemObject {

	protected JiraRestClient restClient;

	public SystemObject(JiraRestClient restClient) {
		this.restClient = restClient;
	}
}
