package com.trendmicro.rdhelpdesk.common.action;

public interface Action {
	public Object find();
}
